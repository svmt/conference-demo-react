import React, {useEffect, useMemo, useRef, useState} from 'react';
import {withRouter} from 'react-router';
import {useSelector} from 'react-redux';
import WT from '@sscale/wtsdk';
import Video from '../Video/Video';
import Footer from '../Footer/Footer';
import './Session.css'


const Session = ({history}) => {
    const {token, userName} = useSelector(state => state.participant);
    const [participants, setParticipants] = useState([]);
    const [connectedToSession, setConnectedToSession] = useState(false);
    const participantsRef = useRef(participants);

    useEffect(() => {
        return () => {
            WT.Session.disconnect();
        }
    }, [])

    useEffect(() => {
        if (!token || !userName) {
            return
        }


        WT.Session.connect(token, userName);

    }, [token, userName]);

    useEffect(() => {
        participantsRef.current = participants;
    }, [participants]);


    useEffect(() => {
        WT.SessionListeners.onConnected((exParticipants) => {

            setConnectedToSession(true);

            WT.SessionListeners.onStreamCreated((params) => {

                const {participantId} = params;

                participantsRef.current = participantsRef.current.filter(participant => participant.participantId !== participantId);

                setParticipants([...participantsRef.current, params])
            });

            WT.ParticipantListeners.onParticipantMediaStreamChanged((params) => {
                const alreadyExists = participantsRef.current.find((p) => p.participantId === params.participantId);
                if (!alreadyExists) {
                    setParticipants([...participantsRef.current, params])
                }
            })

            WT.ParticipantListeners.onParticipantLeft(({participantId}) => {
                setParticipants(participantsRef.current.filter(p => p.participantId !== participantId));
            });

        });
    }, []);


    useEffect(() => {

        const updateWindowDimensions = () => {
            const elem = document.getElementById('videos');
            const width = window.innerWidth;
            const height = window.innerHeight;

            const maxWidth = width - 20;
            const maxHeight = height - 200;

            elem.style.width = `${maxWidth}px`;
            elem.style.height = `${maxHeight}px`;

            if (participants.length === 1) {
                const cont = document.getElementById('video-containers');
                const participant = participants[0];

                cont.style.width = `${maxWidth - (maxWidth / 3)}px`;
                cont.style.height = `${maxHeight - 40}px`;

                const participantContainer = document.getElementById(participant.participantId);

                if (participantContainer) {
                    participantContainer.style.width = 'inherit'
                    participantContainer.style.height = 'inherit'
                }
            } else if (participants.length === 2) {
                const cont = document.getElementById('video-containers');

                cont.style.width = `${maxWidth}px`;
                cont.style.height = `${maxHeight}px`;
                participants.forEach((participant) => {
                    const participantContainer = document.getElementById(participant.participantId);

                    if (participantContainer) {
                        participantContainer.style.width = `${(maxWidth - 40) / 2}px`
                        participantContainer.style.height = `${(maxHeight - 20)}px`
                    }
                });
            } else if (participants.length > 2 && participants.length <= 4) {
                const cont = document.getElementById('video-containers');
                const contW = maxWidth - (maxWidth / 3);
                cont.style.width = `${contW}px`;
                cont.style.height = `${maxHeight}px`;

                participants.forEach((participant) => {
                    const participantContainer = document.getElementById(participant.participantId);

                    participantContainer.style.width = `${(contW - 40) / 2}px`
                    participantContainer.style.height = `${(maxHeight - 40) / 2}px`
                });
            } else if (participants.length > 4 && participants.length <= 6) {
                const cont = document.getElementById('video-containers');
                cont.style.width = `${maxWidth}px`;
                cont.style.height = `${maxHeight}px`;

                participants.forEach((participant) => {
                    const participantContainer = document.getElementById(participant.participantId);

                    participantContainer.style.width = `${(maxWidth - 60) / 3}px`
                    participantContainer.style.height = `${(maxHeight - 40) / 2}px`
                });
            } else if (participants.length > 6 && participants.length <= 9) {
                const cont = document.getElementById('video-containers');
                cont.style.width = `${maxWidth}px`;
                cont.style.height = `${maxHeight}px`;

                participants.forEach((participant) => {
                    const participantContainer = document.getElementById(participant.participantId);

                    participantContainer.style.width = `${(maxWidth - 60) / 3}px`
                    participantContainer.style.height = `${(maxHeight - 60) / 3}px`
                });
            }
        }

        window.addEventListener('resize', updateWindowDimensions);
        updateWindowDimensions();
        return () => window.removeEventListener('resize', updateWindowDimensions)

    }, [window.innerWidth, window.innerHeight, participants])

    const memoizedRenderParticipant = useMemo(() => {
        return (participants.map(participant => {
            return (
                <Video key={participant.participantId} participant={participant}/>
            )
        }))
    }, [participants]);

    const publishParticipant = (participantId) => {
        WT.Participant.publishRemoteParticipant(participantId);
    };

    const disconnectRemoteParticipant = (participantId) => {
        WT.Participant.disconnectRemoteParticipant(participantId)
    }

    return (
        <div className='session-container'>
            <div className='session-videos'>

                <div id='videos' className='video-spaces'>
                    <div id='video-containers'>
                        {memoizedRenderParticipant}
                    </div>

                </div>

            </div>
            <Footer localParticipant={participants.filter(p => !!p.local)[0]}
                    participantPublished={true}
                    connectedToSession={connectedToSession}
                    participants={participants}
            />
        </div>)
};

export default withRouter(Session)
