import React, {useState} from 'react';
import WT from '@sscale/wtsdk';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';

const FooterControlsButton = ({participant}) => {
  const [remoteAudioEnabled, setRemoteAudioEnabled] = useState(true);
  const [remoteVideoEnabled, setRemoteVideoEnabled] = useState(true);

  const toggleAudio = () => {
    WT.Participant.toggleRemoteAudio(participant.participantId);
    setRemoteAudioEnabled(WT.Participant.isRemoteAudioEnabled(participant.participantId));
  }

  const toggleVideo = () => {
    WT.Participant.toggleRemoteVideo(participant.participantId);
    setRemoteVideoEnabled(WT.Participant.isRemoteVideoEnabled(participant.participantId));
  }

  return (<div className='footer-controls'>

    <div className='controls-buttons'>

      <div onClick={toggleAudio}>
        {remoteAudioEnabled && <VolumeUpIcon htmlColor='#ffffff' fontSize='large'/>}
        {!remoteAudioEnabled && <VolumeOffIcon htmlColor='#ffffff' fontSize='large'/>}
      </div>

      <div onClick={toggleVideo}>
        {remoteVideoEnabled && <VideocamIcon htmlColor='#ffffff' fontSize='large'/>}
        {!remoteVideoEnabled && <VideocamOffIcon htmlColor='#ffffff' fontSize='large'/>}
      </div>
    </div>
  </div>)
};

export default FooterControlsButton
