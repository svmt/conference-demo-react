import {combineReducers} from 'redux';
import ParticipantReducer from './ParticipantReducer';

export const rootReducer = combineReducers({
  participant: ParticipantReducer
});
