const SET_ROOM_SETTINGS = 'SET_ROOM_SETTINGS';
const RESET_STORE = 'RESET_STORE';

const initialState = {
  token: '',
  userName: '',
};

function login(userName, streamingToken) {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: SET_ROOM_SETTINGS,
        payload: { token: streamingToken, userName}
      })
    } catch (e) {
      console.error('Error: ', e);
    }
  }
}

function resetStore() {
  return {
    type: RESET_STORE,
    payload: {
      token: '',
      username: '',
    }
  }
}

export {
  login,
  resetStore,
}

const ACTION_HANDLERS = {
  [SET_ROOM_SETTINGS]: (state, action) => {
    const {token, userName} = action.payload;
    return {...state, token, userName}
  },

  [RESET_STORE]: (state, action) => {
    return {...state, ...action.payload}
  }
};

export default function ParticipantReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
