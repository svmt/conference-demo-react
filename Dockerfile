# syntax=docker/dockerfile:experimental
FROM node:10.17-alpine

RUN apk add --no-cache openssh-client git

RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
RUN --mount=type=ssh git clone git@bitbucket.org:svmt/sceenicwebsdk.git sceenicwebsdk

# RUN yarn cache dir
ARG SDK_BRANCH=master
RUN echo "Building SDK ${SDK_BRANCH} branch"
RUN --mount=type=cache,target=/root/.npm \
    --mount=type=cache,target=/usr/local/share/.cache/yarn/v6 \
  cd sceenicwebsdk \
  && git checkout ${SDK_BRANCH} \
  && yarn install \
  && yarn run build \
  && npm pack

RUN cd sceenicwebsdk && mv $(ls | grep sscale-wtsdk) /sscale-wtsdk.tgz
# RUN ls /

COPY . /app
WORKDIR /app
RUN --mount=type=cache,target=/root/.npm \
    --mount=type=cache,target=/usr/local/share/.cache/yarn/v6 \
    yarn install \
    && yarn add /sscale-wtsdk.tgz \
    && yarn run build

FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY --from=0 /app/build /usr/share/nginx/html
